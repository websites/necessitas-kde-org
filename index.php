<?php
    $page_title = "Welcome to KDE Necessitas project";
    include ( "header.inc" );
?>
<p>
Necessitas project was <a href="http://blog.qt.io/blog/2012/11/08/necessitas-android-port-contributed-to-the-qt-project/">completely merged</a> into Qt project and <a href="https://www.kde.org/community/whatiskde/kdefreeqtfoundation.php">KDE Free Qt Foundation Agreement</a> was <a href="http://www.kdab.com/be-free-qt-on-android/">amended</a> to cover <a href="http://blog.qt.io/blog/2013/12/03/kde-free-qt-foundation-agreement-amended-to-cover-qt-for-android-port/">Qt for Android Port</a>.
<br />
We suggest you all to to switch to Qt5.
</p>
<?php
    kde_necessitas_news();
    include ( "footer.inc" );
?>
