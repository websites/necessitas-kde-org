<?php
    $page_title = "Get involved";
    include ( "header.inc" );
?>
<h2>There are two ways to get involved with Necessitas project:</h2>
<h3>1. Make donations</h3>
If you don't have enough time to spend on this project, but you want to show us your support and appreciation you can do it by making donations. Necessitas projects themselves may be totally free, but there are some things that we need that require money, like servers, internet bandwidth, etc. Necessitas is a <b>KDE</b> project, so, to make donations please use <a href="http://www.kde.org/community/donations/">KDE's donation system</a>.
<h3>2. Become a contributor</h3>
<p>
To become a contributor you don't need to be a genius, but you must have <b>good</b> C++ and Qt skills.
</p>
<p>
First and foremost please read the <a href="http://www.kde.org/code-of-conduct/">Code Of Conduct</a>. It is very important also to follow it ! :)
</p>
<p>
Necessitas project has a lot of repositories, so the next step is to read the <a href="http://community.kde.org/Necessitas/Repositories">repositories clarification</a> wiki page.
</p>
<p>
The next step will be to decide how you'll going to help:
<ul>
<li>The easiest way to get used with Necessitas internals/code is to try to fix <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&product=Necessitas&list_id=157757">existing bugs</a>.
<li>After you get used with Necessitas you can pick any open job(s) form our <a href="http://community.kde.org/Necessitas/TODO">to do list</a>.
</ul>
</p>
<p>
The final step will be to contact us and let us what you decide to code:
<ul>
<li> using the <a href="http://mail.kde.org/mailman/listinfo/necessitas-devel">development mailing list</a> (the preferred method).
<li> using <a href=""irc://irc.freenode.net/necessitas">IRC channel</a>, <a href="http://webchat.freenode.net/?channels=necessitas">IRC Webchat</a>
</ul>
</p>
<?php
    include ( "footer.inc" );
?>
