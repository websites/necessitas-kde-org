<?php

// promote title to use
$site_title = "KDE Necessitas project";
$site_logo = "pics/icon.png";
$site_search = false;
$site_menus = 1;
$templatepath = "chihuahua/";

$site_external = true;
$name = "necessitas.kde.org Webmaster";
$mail = "bogdan@kde.org";
$showedit = false;

$rss_feed_link = "/rss.php";
$rss_feed_title = "Latest Necessitas News";

include( "necessitas_functions.inc" );
?>
