<?php
    $page_title = "Getting help from our community resources";
    include ( "header.inc" );
?>
<h3>Find the help you need or help others using the following resources:</h3>
<ul>
<li> <a href="https://groups.google.com/group/android-qt">General propose mailing list</a>
<li> <a href="https://bugs.kde.org/enter_bug.cgi?bug_severity=normal&op_sys=Android%204.x&product=Necessitas&rep_platform=Android&version=alpha4&format=guided">Report a new bug</a>
<li> IRC. You can find us on <a href="irc://irc.freenode.net/necessitas">#necessitas</a> channel in the <a href="http://www.freenode.net/">freenode</a> IRC Network (alias irc.kde.org). You can also try the <a href="http://webchat.freenode.net/?channels=necessitas">IRC WebChat<a/>
<li> Wikis
<ul>
    <li> <a href="http://community.kde.org/Necessitas">KDE Community Wiki</a>
    <li> <a href="http://techbase.kde.org/Development/Tutorials/Necessitas">KDE techbase Wiki</a>
    <li> <a href="http://qt-project.org/wiki/Category:Developing_with_Qt::Ports::Android">qt-project.org Developing with Qt -> Ports -> Android</a>
    <li> <a href="http://qt-project.org/wiki/Category:Developing_Qt::Ports::Android">qt-project.org Developing Qt -> Ports -> Android</a>
</ul>
</ul>
<?php
    include ( "footer.inc" );
?>
