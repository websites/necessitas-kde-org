<?php
    $page_title = "Maintainers of Necessitas";
    include ( "header.inc" );
    maintainerTableHeader();

    maintainerTableEntry("Qt4 framework", "<b>core team</b>", "");
    maintainerTableEntry("Qt4 Mobility", "<b>core team</b>", "");
    maintainerTableEntry("Qt4 WebKit", "<b>core team</b>", "");
    maintainerTableEntry("QtCreator", "<b>core team</b>", "");
    maintainerTableEntry("SDK Installer", "<b>core team</b>", "");
    maintainerTableEntry("Ministro", "<b>core team</b>", "");

    maintainerTableFooter();
?>
<?php
    include ( "footer.inc" );
?>
