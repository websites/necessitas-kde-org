<?php
$page_title = "People behind Necesitas";
include ( "header.inc" );

function person($name, $text, $icon="empty.png")
{
    echo '<p style="clear:left">';
    echo ' <img width="50" vspace="8" hspace="8" src="pics/people/'.$icon.'" alt="'.$name.'" style="float:left" />';
    echo '<br/>';
    echo ' <b>'.$name.'</b>&nbsp;'. $text;
    echo '</p><br/>';
}
echo '<p><h1 style="clear:left">Active members (Alphabetic ordering)</h1>';
person('Marco Bernasocchi','Necessitas website & wiki maintainer');
person('Ray Donnelly','Windows & Mac OSX ports of Necessitas SDK (Installer Framework, QtCreator, Qt framework tools), Necessitas custom toolchains and gdb with python support');
person('Willy Gardiol','Necessitas website & wiki maintainer');
person('Tyler Mandry','reworked Java/C++ interaction, added support for QML debugging on Android');
person('BogDan Vatra', 'is the creator and the current leader of the Necessitas project, is working on: Qt port on Android, QtCreator Android plugin, Necessitas SDK Installer Framework (based on Nokia\'s Installer Framework, Ministro distribution system, and maintains QtMobility');

echo '<h1 style="clear:left">Non active members (Alphabetic ordering)</h1>';
person('Damien Buhl','initial author of Necessitas website, docs and wikis');
person('Elektrobit team','they\'ve made the initial QtMobility port');
person('Marijn Kruisselbrink','worked on Android native menu support');
person('Lauri Laanmets','Android Bluetooth implementation');
person('Nuno Pinheiro','extremely beautiful artwork');
person('Thomas Senyk','OpenGL support for Android');

echo '<h1 style="clear:left">Supporting members</h1>';
person('KDE','keeps all the things running.');

echo '</p>TODO add all the people who contribute with more than a simple patch to this list';

include ( "footer.inc" );
?>
