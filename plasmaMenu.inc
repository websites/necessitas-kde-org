<?php
    global $plasmaMenu;
    $plasmaMenu->addMenu("Necessitas suite", "/index.php", "red.icon.png", "#ff96af");
        $plasmaMenu->addMenuEntry("Download SDK Installer", "/necessitas/necessitas_sdk_installer.php");
        $plasmaMenu->addMenuEntry("Ministro", "/necessitas/ministro.php");
        $plasmaMenu->addMenuEntry("QtCreator", "/necessitas/qtcreator.php");
        $plasmaMenu->addMenuEntry("Qt4", "/necessitas/qt4.php");
            $plasmaMenu->addSubMenuEntry("Qt4 framework","/necessitas/qt4_framework.php");
            $plasmaMenu->addSubMenuEntry("Qt4 Mobility","/necessitas/qt4_mobility.php");
            $plasmaMenu->addSubMenuEntry("Qt4 Webkit","/necessitas/qt4_webkit.php");
        $plasmaMenu->addMenuEntry("Qt5", "http://qt-project.org/wiki/Qt5ForAndroid");

    $plasmaMenu->addMenu("Get involved", "/getinvolved.php", "purple.icon.png", "#e285ff");
        $plasmaMenu->addMenuEntry("Donate", "http://www.kde.org/community/donations/");
        $plasmaMenu->addMenuEntry("Code Of Conduct", "http://www.kde.org/code-of-conduct/");
        $plasmaMenu->addMenuEntry("Repositories clarification", "http://community.kde.org/Necessitas/Repositories");
        $plasmaMenu->addMenuEntry("Check open bugs","https://bugs.kde.org/buglist.cgi?list_id=157124&query_format=advanced&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&product=Necessitas");
        $plasmaMenu->addMenuEntry("To do list", "http://community.kde.org/Necessitas/TODO");
        $plasmaMenu->addMenuEntry("Development mailing list","http://mail.kde.org/mailman/listinfo/necessitas-devel");
        $plasmaMenu->addMenuEntry("IRC WebChat","http://webchat.freenode.net/?channels=necessitas");
        $plasmaMenu->addMenuEntry("IRC channel","irc://irc.freenode.net/necessitas");
        $plasmaMenu->addMenuEntry("3rd party SDK repos","http://community.kde.org/Necessitas/3rd_party_SDK_repositories");

    $plasmaMenu->addMenu("Get help", "/gethelp.php", "red.icon.png", "#ff96af");
        $plasmaMenu->addMenuEntry("Code Of Conduct", "http://www.kde.org/code-of-conduct/");
        $plasmaMenu->addMenuEntry("General mailing list","https://groups.google.com/group/android-qt");
        $plasmaMenu->addMenuEntry("Report a new bug","https://bugs.kde.org/enter_bug.cgi?bug_severity=normal&op_sys=Android%204.x&product=Necessitas&rep_platform=Android&version=alpha4&format=guided ");
        $plasmaMenu->addMenuEntry("Wiki pages","");
            $plasmaMenu->addSubMenuEntry("Main wiki page", "http://community.kde.org/Necessitas");
            $plasmaMenu->addSubMenuEntry("Tutorials", "http://techbase.kde.org/Development/Tutorials/Necessitas");
            $plasmaMenu->addSubMenuEntry("qt-project","http://qt-project.org/wiki/How_to_Create_and_Run_Qt_Application_for_Android");
        $plasmaMenu->addMenuEntry("IRC", "");
            $plasmaMenu->addSubMenuEntry("IRC WebChat","http://webchat.freenode.net/?channels=necessitas");
            $plasmaMenu->addSubMenuEntry("IRC channel","irc://irc.freenode.net/necessitas");

    $plasmaMenu->addMenu("About Necessitas", "", "blue.icon.png", "#ff96af");
        $plasmaMenu->addMenuEntry("News", "/news.php");
        $plasmaMenu->addMenuEntry("People","/people.php");
        $plasmaMenu->addMenuEntry("Donate", "http://www.kde.org/community/donations/");
        $plasmaMenu->addMenuEntry("Join The Game", "http://jointhegame.kde.org/");

    $plasmaMenu->addMenu("About KDE", "http://www.kde.org/community/whatiskde/", "gray.icon.png", "#aaa");
        $plasmaMenu->addMenuEntry("Software Compilation", "http://www.kde.org/community/whatiskde/softwarecompilation.php");
        $plasmaMenu->addMenuEntry("Project Management", "http://www.kde.org/community/whatiskde/management.php");
        $plasmaMenu->addMenuEntry("Development Model", "http://www.kde.org/community/whatiskde/devmodel.php");
        $plasmaMenu->addMenuEntry("Internationalization", "http://www.kde.org/community/whatiskde/i18n.php");
        $plasmaMenu->addMenuEntry("KDE e.V. Foundation", "http://ev.kde.org");
        $plasmaMenu->addMenuEntry("Free Qt Foundation", "http://www.kde.org/community/whatiskde/kdefreeqtfoundation.php");
        $plasmaMenu->addMenuEntry("History", "http://www.kde.org/community/whatiskde/../history/");
        $plasmaMenu->addMenuEntry("Awards", "http://www.kde.org/community/whatiskde/../awards/");
        $plasmaMenu->addMenuEntry("Press Contact", "http://www.kde.org/community/whatiskde/../../contact/");
        $plasmaMenu->addMenuEntry("Donate", "http://www.kde.org/community/donations/");
        $plasmaMenu->addMenuEntry("Join The Game", "http://jointhegame.kde.org/");
        $plasmaMenu->addMenuEntry("Announcements", "http://www.kde.org/announcements/");
        $plasmaMenu->addMenuEntry("Events", "http://events.kde.org/upcoming.php");
        $plasmaMenu->addMenuEntry("Get Involved", "http://www.kde.org/community/getinvolved/");
        $plasmaMenu->addMenuEntry("Press Page", "http://www.kde.org/presspage/");
?>
