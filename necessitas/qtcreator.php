<?php
    $page_title = "QtCreator";
    include ( "header.inc" );
?>
<h1>Qt Creator: The Cross-Platform Qt IDE</h1>

<p>Qt Creator is a cross-platform integrated development environment (IDE) tailored to the needs of Qt developers.  Qt Creator runs on Windows, Linux/X11 and Mac OS X desktop operating systems, and allows developers to create applications for multiple desktop and mobile device platforms.</p>

<p>Necessitas uses a customized version of QtCreator which includes all the required magic to seamlessly integrate Android applications with the C++ world of Qt.</p>

<p>Please refer to the Qt Creator wiki page for download and setup instructions: <a href="http://community.kde.org/Necessitas/Qt4Creator">QtCreator wiki page</a>. Also, please note that Necessitas Qt Creator is part of the <a href="/necessitas/necessitas_sdk_installer.php">Necessitas SDK</a>, so if you have the SDK installed you probably already have everything in place, ready to use!</p>
<?php
    include ( "footer.inc" );
?>
