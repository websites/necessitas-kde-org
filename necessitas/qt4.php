<?php
    $page_title = "Qt4 components";
    include ( "header.inc" );
?>
<p>
<strong>Necessitas suite</strong> provides with the following Qt4 components:
<ul>
<li> <a href="/necessitas/qt4_framework.php">Qt4 framework</a>
<li> <a href="/necessitas/qt4_mobility.php">Qt4 mobility</a>
<li> <a href="/necessitas/qt4_webkit.php">Qt4 WebKit</a>
</ul>
Check the <a href="http://community.kde.org/Necessitas/Qt4">Qt4 wiki page</a> for more information.
</p>
<?php
    include ( "footer.inc" );
?>
