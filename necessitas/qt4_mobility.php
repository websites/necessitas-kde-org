<?php
    $page_title = "Qt4 mobility";
    include ( "header.inc" );
?>

<p>Given the different range of hardware found on today's mobile handsets, Qt4 (Qt5 will have everything built-in by default) needed to be extended with a dedicated hardware abstraction layer. This layer is called <b>QtMobility</b> and has been developed by Nokia.</p>

<p>QtMobility gives you a straight forward way to interface with hardware which is not common on the traditional desktop world such as:
<ul>
<li><i>Connectivity</i>: The Connectivity API facilitates communication with local devices.</il>
<li><i>Contacts</i>: An API enabling clients to request contact data from local or remote backends.</il>
<li><i>Document Gallery</i>: An API to navigate and query documents using their meta-data.</il>
<li><i>Feedback</i>: An API enabling a client to provide tactile and audio feedback to user actions.</il>
<li><i>Location API</i>: The Location API provides a library for location positioning, landmark management and mapping and navigation.</il>
<li><i>Messaging</i>: The Messaging API enables access to messaging services.</il>
<li><i>Multimedia</i>: Provides a set of APIs to play and record media, and manage a collection of media content.</il>
<li><i>Organizer</i>: An API enabling clients to request calendar, schedule and personal data from local or remote backends.</il>
<li><i>Publish and Subscribe</i>: The Publish and Subscribe API, containing Value Space, enables applications to read item values, navigate through and subscribe to change notifications.</il>
<li><i>Sensors</i>: The Sensors API provides access to sensors.</il>
<li><i>System Information</i>: A set of APIs to discover system related information and capabilities.</il>
<li><i>Versit</i>: An API to import and export to the vCard and iCalendar formats.</il>
</ul></p>

<p>QtMobility on Android is far for being completely ported, but already gives you the opportunity to support many areas like Location (GPS) and more.</p>

<p>If you are interested in checking it's status or get your hands dirty with the code, head to <a href="http://community.kde.org/Necessitas/CompileQtFramework#QtMobility">the build QtMobility page</a> and have fun! Other users should just <a href="http://community.kde.org/Necessitas/InstallSDK">install the Necessitas SDK</a> which will provide you with all the needed components ready to use!</p>


<?php
    include ( "footer.inc" );
?>
