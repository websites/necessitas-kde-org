<?php
    $page_title = "Qt4 WebKit";
    include ( "header.inc" );
?>

<p>QtWebKit provides a Web browser engine that makes it easy to embed content from the World Wide Web into your Qt application. At the same time Web content can be enhanced with native controls. QtWebKit provides facilities for rendering of HyperText Markup Language (HTML), Extensible HyperText Markup Language (XHTML) and Scalable Vector Graphics (SVG) documents, styled using Cascading Style Sheets (CSS) and scripted with JavaScript.</p>

<p>A bridge between the JavaScript execution environment and the Qt object model makes it possible for custom QObjects to be scripted. Integration with the Qt networking module enables Web pages to be transparently loaded from Web servers, the local file system or even the Qt resource system. In addition to providing pure rendering features, HTML documents can be made fully editable to the user through the use of the contenteditable attribute on HTML elements. QtWebKit is based on the Open Source WebKit engine.</p>

<p>QtWebKit for Android let's you embedd into your Qt based applications very easily both a powerfull HTML-capable viewer and editor. You can embedd with a few lines of code your internal web browser (to keep users within your boudnaries, or to display inline some online help documents)...</p>

<p>QtWebKit is integrated into the Qt Framework for Android, so just grab the <a href="http://community.kde.org/Necessitas/InstallSDK">Necessitas SDK</a> and start using it... Or get your hands dirty and <a href="http://community.kde.org/Necessitas/CompileQtFramework">build your own</a> if you want to modify it!</p>

<?php
    include ( "footer.inc" );
?>
