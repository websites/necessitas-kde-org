<?php
    $page_title = "Download Information";
    include ( "header.inc" );
?>
<p>The preferred way of installing Necessitas suite is by using the <b>Necessitas SDK Installer-Framework</b> for the following platforms:
<ul>
 <li><a href="http://files.kde.org/necessitas/installer/release/linux-online-necessitas-alpha4-sdk-installer">GNU/Linux (32/64)</a>
 <li><a href="http://files.kde.org/necessitas/installer/release/mac_osx-online-necessitas-alpha4-sdk-installer.7z">Mac OSX (32/64)</a>
 <li><a href="http://files.kde.org/necessitas/installer/release/windows-online-necessitas-alpha4-sdk-installer.exe">Windows (32/64)</a>
</ul>

<p>Unless you are planning to modify Necessitas, the Android Qt framework or doing something else more complex, you will want to download the Necessitas SDK and start from there.</p>

<p>The installer will install all the components you need to begin to create Qt apps for Android in minutes. The preferred development platform is Linux, but the installer is available also for Windows and MacOSX. If you have to set up a new development, it's recomended to use Linux, but if you have to stick with Windows or MacOSX, we got you covered too!</p>
<br/>
Check the <a href="http://community.kde.org/Necessitas/InstallSDK">SDK installer wiki page</a> for more information, requirements and detailed installation instructions.
</p>
<?php
    include ( "footer.inc" );
?>
