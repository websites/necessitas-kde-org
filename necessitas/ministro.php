<?php
    $page_title = "Ministro";
    include ( "header.inc" );
?>
<p><strong>Ministro</strong> is a system wide libraries installer/provider for Android.
<br/>
 It is one of most critical piece of this project, without it this project is almost worthless, this installer/provider is the guarantee that all your apps will run on <strong>ALL</strong> android platforms WITHOUT any modification!
<br/>
How it works:
<ul>
  <li>when your application starts, it searches for Ministro service, if the service is not installed, your applications will open the Android Market and request user to install it (it also starts the search for the user).
  <li> (after the service is installed) the application connects to Ministro service, it  gives a list of qtmodules and waits (patiently :) ) for Ministro's callback.
  <li> Ministro downloads all necessary libraries (only if they are missing), and callbacks the application and gives it a list with all necessary libraries to run.
  <li> the application loads all libraries provided by Ministro service and finally it starts.
</ul>

You can find Minsitro on the <a href="https://play.google.com/store/apps/details?id=org.kde.necessitas.ministro">Android Play</a> or you can download it from <a href="http://files.kde.org/necessitas/installer/release/Ministro%20II.apk">here</a>.

<br/>
<br/>
For more information about Ministro, please check the <a href="http://community.kde.org/Necessitas/Ministro">Ministro wiki page</a>.
</p>
<?php
    include ( "footer.inc" );
?>
