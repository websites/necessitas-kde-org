<?php
    $page_title = "Qt4 framework";
    include ( "header.inc" );
?>

<p>Qt is a cross-platform application and UI framework with APIs for C++ programming and Qt Quick for rapid UI creation.</p>

<p>Among it's features, you can find:
<ul>
<li>Intuitive class libraries</li>
<li>Easy to use and learn</li>
<li>Produce highly readable, easily maintainable and reusable code</li>
<li>Complete tool set (i18n, hardware interfaces, full libraries, ...)</li>
<li>Multi-platform: Windows, Linux, MacOSX, Symbian, Android, iOS, BlackBerry, Maemo, MeeGo...</li>
</ul></p>

<p>Qt powers some of the most advanced as well some of the simplest softwares... The KDE Desktop Environment is based on the Qt, as well as many mobile applications. Qt is powerful and simple at the same time. It let's you develop easily and quickly something really small and, at the sate time, gives you all the power and tool that you need to create and manage huge software products. Don't be scared, extensive very detailed help system and a huge amount of source code, real-world, examples make it a breeze to begin and to dig deeper! Forget about hidden APIs, convoluted interfaces, not well documented features. Qt is another planet. And it's released under LGPL too!</p>

<p>Thanks to Necessitas all this is now available for Android as well. If you are a new user and you want to start playing with Qt for Android, you should start with <a href="/necessitas/necessitas_sdk_installer.php">installing the Necessitas SDK</a>. If you are looking to extend, browse or just play with the Qt framework itself, jump to <a href="http://community.kde.org/Necessitas/CompileQtFramework">the build Android Qt framework instructions</a> and get your hands dirty!</p>
<?php
    include ( "footer.inc" );
?>
