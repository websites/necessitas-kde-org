<?php
	function addGroup( $name, $link )
	{
		echo "<h3 class='programboxh3'><a href='$link'>$name</a></h3>";
	}
	
	function addProgram( $name, $link, $pic )
	{
		echo "<div class='programbox'>\n";
		echo "<div align='center'><a href='$link'>\n";
		echo "<img src='$pic' width='32' height='32' border='0' hspace='10' vspace='10' alt='$name' title='$name' />\n";
		echo "</a></div>\n";
		echo "<div class='programbox-text'><a href='$link'>$name</a></div>\n";
		echo "</div>\n";
	}
	
	addGroup("Project", "project");
	addProgram("KDEwin-Installer", "installer", "pics/projects/hi32-app-kanagram.png");
?>

<div style="clear: both;"></div>
