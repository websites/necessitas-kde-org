<?php
    include_once ("functions.inc");
    $rss_feed_link = "http://necessitas.kde.org/rss.php";
    $rss_feed_title = "Latest News";
    include('includes/header.inc');
    // use local copies of template files
    unset($templatepath);
    // set additional trademark which is displayed in the local template-bottom2.inc
    $trademarks = "Android is a trademark of Google Inc";
?>
